import React from "react";
import { StyleSheet, TextInput, View } from "react-native";
import {Ionicons} from '@expo/vector-icons'


function SearchBox(){
    <View style={styles.rootContainer}>
        <Ionicons name="search" size={24}/>
        <TextInput placeholder="Search"/>
    </View>
}

export default SearchBox

const styles = StyleSheet.create({
    rootContainer:{
        flexDirection: 'row',
        borderRadius: 2,
        width: '100%',
        marginHorizontal: 5,
        marginVertical: 10
    }
})