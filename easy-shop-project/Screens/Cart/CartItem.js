import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Text, Left, Right, ListItem, Thumbnail, Body } from
    "native-base";
const CartItem = (props) => {
    const data = props.item.item;
    const [quantity, setQuantity] = useState(props.item.item.quantity);
    return (
        <ListItem style={styles.listItem} key={Math.random()} avatar>
            <Left>
                <Thumbnail
                    source={{
                        uri: data.product.image
                            ? data.product.image
                            :
                            "https://cdn.pixabay.com/photo/2016/12/05/10/07/dish-1883501_1280.png",
                    }}
                />
            </Left>
            <Body style={styles.body}>
                <Left>
                    <Text>{data.product.name}</Text>
                </Left>
                <Right>
                    <Text>$ {data.product.price}</Text>
                </Right>
            </Body>
        </ListItem>
    );
};
export default CartItem;

const styles = StyleSheet.create({
    listItem: {
        alignItems: "center",
        backgroundColor: "white",
        justifyContent: "center",
    },
    body: {
        margin: 10,
        alignItems: "center",
        flexDirection: "row",
    },
});